const a = 1;
const b = null;

console.log(a ?? 'no value');
console.log(b ?? 'no value');

const address1 = {
    text: 'г. Казань, ул. Кремлевская, д. 35, ауд. 1509',
    city: {
        title: 'Казань'
    },
    house: {
        number: 35
    },
    street: {
        title: 'Кремлевская'
    },
    room: {
        number: 1509,
        type: 'аудитория'
    }
};

const address2 = {
    text: 'г. Казань, ул. Центральная (пос. Салмачи), д. 123',
    city: {
        title: 'Казань'
    },
    street: {
        title: 'Центральная (пос. Салмачи)'
    },
    house: {
        number: 123
    }
};
console.log(address2);

function getRoomFromAddress(address) {
    // вместо
    // if (
    //     address.room !== undefined &&
    //     address.room.type !== undefined &&
    //     address.room?.type?.mainTitle !== undefined
    // ) {
    //     return address.room.type.mainTitle.ru;
    // }
    // else {
    //     return '';
    // }

    // пишем:
    return `${address.room?.type?.mainTitle?.ru ?? ''} ${address.room?.number ?? ''}`;
}

console.log(getRoomFromAddress(address1));
console.log(getRoomFromAddress(address2));
