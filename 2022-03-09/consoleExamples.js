console.debug('debug in console');
console.log('print in console');
console.info('console info');
console.warn('console warn');
console.error('console info');

console.log(window);
console.dir(window);


const cat = {
    name: "Барсик",
    color: "black",
    age: 5,
    meow() {
        alert('MEOW!');
    }
}

const cat2 = {
    name: "Олег",
    color: "white",
    age: 8,
    meow() {
        alert('OLEG!');
    }
}

console.dir(cat);

console.table(cat);
console.table([cat, cat2]);
