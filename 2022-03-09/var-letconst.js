function varFunction() {
    var oldVar = 1;
    if (true) {
        var oldVar2 = 2;
    }
    console.log('oldVar', oldVar); // 1
    console.log('oldVar2', oldVar2); // 2

    let letVar = 1;
    if (true) {
        let letVar2 = 2;
        console.log('letVar2 inside if', letVar2); // undefined
    }
    console.log('letVar', letVar); // 1
    console.log('letVar2', letVar2); // undefined

    const constVar = 1;
    if (true) {
        const constVar2 = 2;
    }
    console.log(constVar); // 1
    console.log(constVar2); // undefined
}

varFunction();

