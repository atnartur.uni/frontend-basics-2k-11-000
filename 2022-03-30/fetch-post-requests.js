async function main() {
    const formData = new FormData();
    formData.append("name", "Вася");
    formData.append("age", 45);

    const response = await fetch(
        "/api",
        {
            method: "POST",
            headers: {
                "Content-type": "application/x-www-form-urlencoded"
            },
            body: formData
        }
    )
    const data = await response.json()
    console.log(data);
}
main();
