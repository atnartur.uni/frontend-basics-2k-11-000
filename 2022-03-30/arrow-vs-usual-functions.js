function User(name) {
  return {
    name,
    // usual function: with context
    getName: function() {
      return this.name;
    }
  }
}

function UserArrow(name) {
  return {
    name,
    // arrow function: without context
    getName: () => {
      return this.name;
    }
  }
}

const user = new User("Вася");
console.log(user.getName());

function x2(number) {
  return number * 2;
}

const x2lambda = number => number * 2;

const a = [1, 2, 3];
console.log(a.map(x2lambda))
