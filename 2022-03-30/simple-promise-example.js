function wait() {
    return new Promise((resolve, reject) => {
        // reject();
        setTimeout(resolve, 1000);
    });
}

console.log('start');

const promise = wait();
console.log(promise);

// promise status: pending
promise.then(function() {
    // promise status: fulfilled
    console.log(123);
}).catch(function () {
    // promise status: rejected
    console.log('error!');
})
