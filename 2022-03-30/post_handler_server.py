from flask import Flask, request, send_from_directory

app = Flask(__name__)


@app.route("/")
def main():
    with open('index.html', 'r') as f:
        return f.read()

@app.route('/<path:path>')
def send_report(path):
    return send_from_directory('.', path)


@app.route("/api", methods=['GET', 'POST'])
def api():
    return {
        "method": request.method,
        "form": request.form,
        "body": request.json
    }

if __name__ == '__main__':
    app.run()
