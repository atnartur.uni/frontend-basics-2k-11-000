async function load(login) {
    const response = await fetch(`https://api.github.com/users/${login}`);
    return await response.json();
}

function showMember(responseContent) {
    const body = document.getElementsByTagName("body")[0];
    body.innerHTML += `<img src="${responseContent.avatar_url}" width="100"> ${responseContent.login} <hr>`
}

function showLoader() {
    document.querySelector('#loader').style.display = 'block';
}
function hideLoader() {
    document.querySelector('#loader').style.display = 'none';
}

async function main() {
    showLoader();
    try {
        const results = await Promise.all([
            load('migueliusoff'),
            load('RaySkarken'),
            load('atnartur')
        ]);
        console.log('promise results', results);
        results.forEach(showMember);
    }
    catch (error) {
        console.error(error);
    }
    hideLoader();
}

document.addEventListener("DOMContentLoaded", () => main());
