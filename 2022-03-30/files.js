// NodeJS module. Run: node files.js
const fs = require('fs');
const fsPromises = require('fs/promises');
const path = require('path');

console.log('start');

const dirPath = "temp";

function writeMyFile(content) {
    const fullPath = path.join(dirPath, 'myfile.txt');
    return fsPromises.writeFile(fullPath, content);
}

function exists(dirPath) {
    return new Promise(resolve => {
        resolve(fs.existsSync(dirPath));
    });
}

function mkdir(dirPath) {
    return fsPromises.mkdir(dirPath);
}

// проверить наличие папки

async function main() {
    const isExists = await exists(dirPath);
    if (!isExists) {
        await mkdir(dirPath);
    }
    await writeMyFile("write after create dir");
    console.log('file write success');

    // exists(dirPath)
    //     .then(isExists => {
    //         if (!isExists) {
    //             const promise = mkdir(dirPath)
    //             console.log(promise)
    //             return promise;
    //         }
    //     })
    //     .then(() => writeMyFile("write after create dir"))
    //     .then(() => console.log('file write success'));

}
main().then(() => console.log('all done'));


// exists(dirPath).then(function (isExists) {
//     console.log(isExists);
//     if (!isExists) {
//         // если папки нет, нужно создать папку
//         mkdir(dirPath).then(function () {
//             console.log('directory created');
//             writeMyFile("write after create dir").then(() => {
//                 console.log('file write success');
//             });
//         });
//     }
//     else {
//         writeMyFile("write without create dir").then(() => {
//             console.log('file write success');
//         });
//     }
// });
