// CONST & LET

const test = 1;

let testVar2 = 1;
testVar2 = 2;

console.log('testVar2', testVar2);

var oldVar = 3;
oldVar = 5
console.log('oldVar', oldVar);

function example() {
	const internalVar = 1;
	console.log('internalVar from function', internalVar);
	var oldVar2 = 2;
	console.log('oldVar2 from function', oldVar2);
}

example();

// console.log('oldVar2 outside function', oldVar2);
// error:
// console.log('internalVar outside function', internalVar);

// TYPES

const testStr = 'string';
console.log(testStr);

const testNumber = 4;
const testNumber2 = 4.2;
console.log(4 ** 2);

Math.floor(4.5)
Math.ceil(4.5)

console.log(parseInt('asdfsd')) // NaN
console.log(parseInt('2'))
console.log(parseFloat('2.3'))

const testBoolVar = false;

let undefinedTest;
console.log(undefinedTest);

let nullTest = null;
console.log(nullTest);

console.log(typeof testNumber2, typeof testBoolVar);

console.log(4 == '4');
console.log(4 === '4');

// OBJECT

const cat = {
	name: 'Барсик',
	color: 'black',
	age: 4,
	favoriteToy: {
		type: 'ball',
		color: 'red'
	},
	printName() {
		console.log(this.name);
	}
};
console.log(cat, typeof cat);

cat.printName();

// ARRAY

const guestList = ['Петя', 'Вася'];
guestList.push('Андрей');
guestList.unshift('Оля');
console.log(guestList, guestList.length);
console.log(guestList[0], guestList[2]);
console.log(guestList[guestList.length - 1])

console.log(guestList.slice(1, 3))

// CONDITIONS

if (cat.name === 'Барсик') {
	console.log(cat.name);
}
else if (cat.name === 'Пушок') {
	console.log('Привееет!');
}
else {
	console.log('непонятно, как зовут кота');
}

// CYCLES

for (let i = 0; i < 5; i++) {
	console.log(i);
}

for (let item of guestList) {
	console.log(item);
}

for (let item in guestList) {
	console.log(item);
}

// error
// for (let item of cat) {
// 	console.log(item);
// }
for (let item in cat) {
	console.log(item, cat[item]);
}

let j = 0;
while (j < 5) {
	console.log(j++);
}

let g = 0;
do {
	console.log(g++);
}
while(g < 5);


// alert, confirm, prompt
alert('Базовое сообщение');
console.log(confirm('Вы уверены?'));
console.log(prompt('Как вас зовут?'));



